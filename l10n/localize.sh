#!/bin/sh
#
# Execute the full internationalization workflow,
# in order to allow localization. Whatever it means.
#

base="$1"
if [ -z "$base" ]; then
	echo "Missing base directory (first argument)"
	exit 1
fi

# Pull stuff from the database and save strings as __( "string" )
# So later GNU Gettext can recognize these strings and put these in the .po later.
php "$base""/l10n/mieti.php" > "$base""/l10n/trebbia.php"

# First run: GNU Gettext generates .po files from source code
php "$base""/l10n/localize.php" "$base"

# Second run: GNU Gettext generates .mo binary files from .po files
php "$base""/l10n/localize.php" "$base"

