<?php
# Linux Day Torino website
# Copyright (C) 2016-2023 Valerio Bozzolan, Linux Day Torino contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require 'load.php';

require_permission( 'see-backend' );

// Check if the desired Conference exists.
$conference_uid = luser_input( $_GET['conference_uid'] ?? '', 50 );
$conference = ( new QueryConference() )
	->whereConferenceUID( $conference_uid )
	->queryRow();

// No Conference, no party.
if( !$conference ) {
	die_with_404();
}

// Get all Events related to this Conference.
$events = ( new QueryEvent() )
	->joinConference()
	->joinChapter( 'LEFT' )
	->joinTrack( 'LEFT' )
	->orderBy( Track::NAME )
	->orderBy( Event::END )
	->whereConference( $conference )
	->queryGenerator();

Header::spawn( null, [
	'title' => __( "Talk" ),
] );
?>

	<!-- start add button -->
	<div class="row">
		<div class="col s12">
			<a class="btn waves-effect purple" href="<?=
				esc_attr( FullEvent::editURL( [
					'new' => 1,
					'conference' => $conference_uid,
				] ) )
			?>"<?= disabled( !has_permission( 'add-event' ) ) ?>><?= __( "Crea" ) . icon( 'save', 'left' ) ?></a>
		</div>
	</div>
	<!-- stop add button -->

	<!-- start list -->
	<div class="row">
		<?php foreach( $events as $event ): ?>
			<div class="col s12">
				<div class="card-panel">
					<h2><?= ( new HTML( 'a' ) )
						->setAttr( 'href', $event->getEventURL() )
						->setText( $event->getEventTitle() )
						->render()
					?></h2>

					<p><code>#<?= $event->getTrackName() ?></code></p>

					<!-- start edit button -->
					<a class="btn waves-effect purple" href="<?= esc_attr( $event->getFullEventEditURL() ) ?>" <?= disabled( !$event->isEventEditable() ) ?>><?= __( "Modifica" ) . icon( 'save', 'left' ) ?></a>
					<!-- stop edit button -->
				</div>
			</div>
		<?php endforeach ?>
	</div>
	<!-- end list -->

<?php
Footer::spawn();
